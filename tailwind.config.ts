import { colord, extend } from "colord";
import mixPlugin from "colord/plugins/mix";
import type { Config } from "tailwindcss";

extend([mixPlugin]);

const generateDarkenColorFrom = (
  inputColor: string,
  percantage: number = 0.07
): string => colord(inputColor).darken(percantage).toHex();

const generateForegroundColorFrom = (
  inputColor: string,
  percantage: number = 0.8
): string =>
  colord(inputColor)
    .mix(colord(inputColor).isDark() ? "white" : "black", percantage)
    .toHex();

export const tailwindColors: { [key: string]: string } = {
  current: "currentColor",
  transparent: "transparent",
  white: "#F9F9F9",
  primary: "#007BEC",
  "primary-content": "#FFFFFF",
  "primary-focus": generateDarkenColorFrom("#007BEC"),
  secondary: "#6C5CE7",
  "secondary-content": "#FFFFFF",
  "secondary-focus": generateDarkenColorFrom("#6C5CE7"),
  accent: "#1FB2A5",
  "accent-content": "#FFFFFF",
  "accent-focus": generateDarkenColorFrom("#1FB2A5"),
  neutral: "#2A323C",
  "neutral-content": generateForegroundColorFrom("#FFFFFF"),
  "neutral-focus": generateDarkenColorFrom("#2A323C", 0.3),
  "base-25": "#353D47",
  "base-50": "#2A323C",
  "base-75": "#20272E",
  "base-100": "#1D232A",
  "base-200": "#191E24",
  "base-300": "#15191E",
  info: "#3ABFF8",
  "info-content": generateForegroundColorFrom("#3ABFF8"),
  success: "#36D399",
  "success-content": generateForegroundColorFrom("#36D399"),
  warning: "#FBBD23",
  "warning-content": generateForegroundColorFrom("#FBBD23"),
  error: "#F87272",
  "error-content": generateForegroundColorFrom("#F87272"),
  "gradient-first": "#34EAA0",
  "gradient-second": "#0FA2E9",
};

const config: Config = {
  content: [
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: tailwindColors,
  },
  plugins: [],
};
export default config;
