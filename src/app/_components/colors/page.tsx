"use client";
import { colord } from "colord";
import { tailwindColors } from "../../../../tailwind.config";

const getTextColor = (inputColor: string): string =>
  colord(inputColor)?.isDark() ? "#DDDDDD" : "#333333";

export const Colors: React.FC = () => {
  return (
    <div lang="en" className="flex flex-wrap justify-center" dir="ltr">
      {Object.entries(tailwindColors)?.map(([name, color]) => (
        <ColorBox key={name} name={name} color={color} />
      ))}
    </div>
  );
};

export const ColorBox: React.FC<{ name: string; color: string }> = ({
  name,
  color,
}) => {
  return (
    <div
      className="w-96 h-60 flex flex-col justify-center items-center text-center uppercase"
      style={{ backgroundColor: color, color: getTextColor(color) }}
    >
      <span>{color}</span>
      <span>{name}</span>
    </div>
  );
};
