import "./globals.css";
import { Figtree } from "next/font/google";
import localFont from "next/font/local";

const figtree = Figtree({
  display: "swap",
  subsets: ["latin"],
  weight: ["300", "400", "500", "600", "700", "800", "900"],
  variable: "--font-figtree",
});

const vazir = localFont({
  src: [
    {
      path: "../../public/fonts/vazir/Vazir-Thin-FD-WOL.woff2",
      style: "normal",
      weight: "100",
    },

    {
      path: "../../public/fonts/vazir/Vazir-Light-FD-WOL.woff2",
      style: "normal",
      weight: "300",
    },

    {
      path: "../../public/fonts/vazir/Vazir-FD-WOL.woff2",
      style: "normal",
      weight: "400",
    },

    {
      path: "../../public/fonts/vazir/Vazir-Medium-FD-WOL.woff2",
      style: "normal",
      weight: "500",
    },

    {
      path: "../../public/fonts/vazir/Vazir-Bold-FD-WOL.woff2",
      style: "normal",
      weight: "700",
    },
  ],
  variable: "--font-vazir",
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html
      lang="fa"
      dir="rtl"
      className={`${figtree.variable} ${vazir.variable}`}
    >
      <body className="flex flex-col min-h-screen font-bold uppercase">
        {/* <header className="bg-gray-200 flex items-center justify-center h-20">
          دوره معماری نکست 1
        </header> */}
        <div className="flex-1 flex">{children}</div>
        {/* <footer className="bg-gray-200 flex items-center justify-center h-20">
          footer
        </footer> */}
      </body>
    </html>
  );
}
