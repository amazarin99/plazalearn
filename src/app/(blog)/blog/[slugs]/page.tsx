export default async function BlogDetails({
  params: { slugs },
}: {
  params: { slugs: string };
}) {
  return (
    <div className="text-5xl flex justify-center items-center w-full">
      {slugs}
    </div>
  );
}
