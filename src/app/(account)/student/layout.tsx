import { ReactNode } from "react";

export default function StudentLayout({ children }: { children: ReactNode }) {
  return (
    <>
      <aside className="bg-gray-300 w-80 flex justify-center items-center uppercase">
        aside
      </aside>
      <main className="flex items-center justify-center w-full">
        {children}
      </main>
    </>
  );
}
