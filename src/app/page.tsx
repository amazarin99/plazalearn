import Image from "next/image";
import { Colors } from "./_components/colors/page";
import developSvg from "../../public/images/web-design-layout.svg";

export default function Home() {
  return (
    <div className="flex justify-center items-center w-full flex-col">
      <div className="w-96 h-96">
        <Image src={developSvg} alt="develop mode" />
      </div>
      <h3 className="text-2xl text-base-100">پروژه در حال توسعه می‌باشد ...</h3>
    </div>
  );
}
