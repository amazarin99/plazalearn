"use client";

export default function Error() {
  return (
    <div className="text-3xl flex justify-center items-center text-red-700 w-full">
      Error
    </div>
  );
}
